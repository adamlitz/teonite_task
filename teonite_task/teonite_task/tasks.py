from celery import shared_task
from scrapyd_api import ScrapydAPI
from uuid import uuid4


@shared_task
def run_spiders():
    print("Running spiders")

    # connect scrapyd service
    scrapyd = ScrapydAPI('http://172.28.1.3:6800')

    settings = {
        'unique_id': str(uuid4()),  # unique ID for each record for DB
        'USER_AGENT': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
    }

    scrapyd.schedule('default', 'blog-spider', settings=settings)
