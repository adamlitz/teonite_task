#!/usr/bin/env sh

# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

# Apply database migrations
echo "Apply database migrations"
python manage.py migrate

# wait for postgres
./wait_for_postgres.sh

# Run
gunicorn --env DJANGO_SETTINGS_MODULE=teonite_task.settings teonite_task.wsgi:application -b :8000
