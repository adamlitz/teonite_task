from collections import Counter
import nltk
from nltk.corpus import stopwords


def remove_stop_words(words):
    """Remove stop words"""
    nltk.download('stopwords')
    stop_words = set(stopwords.words('english'))
    cleared_words = [word for word in words if word.lower() not in stop_words]

    return cleared_words


def get_most_common_words(queryset):
    """Get top 10 common words from text"""
    words_list = []
    for article in queryset:
        words_list.extend(list(article.content.split(' ')))

    cleared_words = remove_stop_words(words_list)
    most_common = Counter(cleared_words).most_common(10)

    return most_common
