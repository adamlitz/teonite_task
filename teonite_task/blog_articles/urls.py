from django.urls import path
from . import views

urlpatterns = [
    path('stats/', views.ArticleListAPIView.as_view(), name="stats"),
    path('stats/<str:author>/', views.AuthorArticlesListAPIView.as_view(), name="author-stats"),
    path('authors/', views.AuthorListAPIView.as_view(), name='authors')

]
