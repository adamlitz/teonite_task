# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html
from blog_articles.models import Article
from scrapy_djangoitem import DjangoItem


class ArticleItem(DjangoItem):
    """Scrapy item to Django model mapping"""
    django_model = Article
