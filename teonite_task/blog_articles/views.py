from rest_framework.generics import ListAPIView, RetrieveAPIView
from .serializers import AuthorSerializer, ArticleSerializer
from .models import Author, Article
from rest_framework.response import Response
from .utils import get_most_common_words


class AuthorListAPIView(ListAPIView):
    """Authors and their nicknames"""
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer_data = self.serializer_class(queryset, many=True).data

        return Response({author['name_lower']: author['name'] for author in serializer_data})


class ArticleListAPIView(ListAPIView):
    """Words count per all articles"""
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        most_common = get_most_common_words(queryset)

        return Response({word[0]: word[1] for word in most_common})


class AuthorArticlesListAPIView(ListAPIView):
    """Words count per users articles"""
    serializer_class = ArticleSerializer

    def get_queryset(self):
        return Article.objects.filter(author__name_lower=self.kwargs['author'])

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        most_common = get_most_common_words(queryset)

        return Response({word[0]: word[1] for word in most_common})
