from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=150, primary_key=True)
    name_lower = models.CharField(max_length=150)


class Article(models.Model):
    """The scrapped content will be saved in this model"""
    author = models.ForeignKey(Author, on_delete=models.CASCADE, db_column='blog_articles_article.author')
    content = models.TextField()
    title = models.CharField(max_length=200)
