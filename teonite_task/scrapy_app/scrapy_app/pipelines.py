# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
from blog_articles.models import Article, Author


class ScrapyAppPipeline:
    """
    Pipeline in which scraped data will be cleared and saved in the db
    """
    def clear_content(self, item):
        """Initial content clearance, removal of end lines and special characters"""
        raw_content = item.get('content')
        title = item.get('title')

        # Consider title also as a content
        raw_content.append(title)

        content = ' '.join(raw_content)
        list_content = list(content.split(" "))

        # Remove all non-alphanumeric words
        final_content = []
        for word in list_content:
            if word.isalnum():
                final_content.append(word)

        # Return content as a string
        return ' '.join(final_content)

    def process_item(self, item, spider):
        """Save scraped content to the db"""
        if not Author.objects.all().filter(name=item.get('author')).exists():
            author = Author(name=item.get('author'),
                            name_lower=item.get('author').lower().replace(" ", ""))
            author.save()

        if not Article.objects.all().filter(title=item.get('title')).exists():
            # We need to create author object due to ForeignKey
            author = Author(name=item.get('author'),
                            name_lower=item.get('author').lower().replace(" ", ""))
            author.save()

            article = Article(author=author,
                              content=self.clear_content(item),
                              title=item.get('title'))
            article.save()

        return item
