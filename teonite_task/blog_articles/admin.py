from django.contrib import admin
from .models import Article, Author


class ArticleAdmin(admin.ModelAdmin):
    pass


class AuthorAdmin(admin.ModelAdmin):
    pass


admin.site.register(Article, ArticleAdmin)
admin.site.register(Author, AuthorAdmin)
