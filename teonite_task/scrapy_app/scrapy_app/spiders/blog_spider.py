import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from ..items import ArticleItem


class BlogSpider(CrawlSpider):
    name = 'blog-spider'
    allowed_domains = ['teonite.com']
    start_urls = ['https://teonite.com/blog/']

    """Rules for finding links on a single page"""
    rules = [Rule(LinkExtractor(unique=True, restrict_css='h2.post-title', tags='a'),
                  callback='parse_article', follow=True)]

    def parse_start_url(self, response):
        """Find all links"""
        next_page_url = response.css('.pagination-list li:nth-child(3) > a::attr(href)').extract_first()

        # If it doesn't exist, we are on the first page
        if next_page_url is None:
            next_page_url = response.css('.pagination-list li:nth-child(2) > a::attr(href)').extract_first()

        return scrapy.Request(response.urljoin(next_page_url))

    def parse_article(self, response):
        """Parse single article and get necessary data"""
        content = response.css('div.col-lg-12')

        item = ArticleItem()
        item['author'] = content.css('.author-name > strong::text').get()
        item['content'] = [text.strip() for text in content.css('.post-content ::text').extract()]
        item['title'] = content.css('.post-title::text').get()

        return item
