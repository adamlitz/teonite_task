from rest_framework.test import APITestCase, APIClient
from .models import Article, Author
from django.urls import reverse
from .utils import remove_stop_words, get_most_common_words


class AuthorsAPIViewTest(APITestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        self.author_1 = Author(name='Jacek Bury', name_lower='jacekbury')
        self.author_1.save()

        self.author_2 = Author(name='Anna Gniazdo', name_lower='annagniazdo')
        self.author_2.save()

    def test_authors_response_format_is_correct(self):
        response = self.client.get(reverse("authors"))
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json(), dict)

    def test_authors_name_is_correct(self):
        response = self.client.get(reverse("authors"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['jacekbury'], 'Jacek Bury')

    def test_authors_users_count_is_correct(self):
        response = self.client.get(reverse("authors"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 2)


class ArticlesAPIViewTest(APITestCase):
    def setUp(self) -> None:
        self.client = APIClient()

    def test_create_article(self):
        author_1 = Author(name='Jacek Bury', name_lower='jacekbury')
        author_1.save()

        article_1 = Article(author=author_1,
                            content='bla bla bla bla bla wu wu wu',
                            title='dunno')
        article_1.save()

        self.assertEqual(Article.objects.filter(title='dunno').exists(), True)

    def test_get_articles_stats(self):
        response = self.client.get(reverse("stats"))
        self.assertEqual(response.status_code, 200)


class ArticlesUtilsTest(APITestCase):
    def test_remove_stop_words(self):
        example_stop_words = ['the', 'is', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours']
        cleared_content = remove_stop_words(example_stop_words)

        self.assertEqual(len(cleared_content), 0)

    def get_most_common_words(self):
        example_sentence = ['Either', 'start', 'list', 'items', 'flush', 'left', 'or',
                            'indent', 'them', 'no', 'more', 'than', 'half', 'an', 'inch',
                            'start', 'start']

        most_common_words = get_most_common_words(example_sentence)

        self.assertEqual(most_common_words[0], 'start')
