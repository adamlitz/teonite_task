# Generated by Django 3.0.7 on 2020-06-08 17:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog_articles', '0002_author_lower_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='author',
            name='lower_name',
        ),
    ]
